package dawson;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }

    //my code starts here
    public int echo(int x){
        return x;
    }

    public int oneMore(int x){
        //purposely putting a bug
        return x;
    }
}
