package dawson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    //creating app object
    App a = new App();

    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    //my code starts here
    @Test
    public void shouldReturnSame(){
        assertEquals( "Returns expected value", 5, a.echo(5));
    }

    //deleted auto generated text
